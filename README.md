# Chilli Source: Gameboy Emulator #

A Gameboy emulator that runs in the Chilli Source free, open-source, game engine (www.chilli-works.com).

### How do I get set up? ###

* Chilli Source is included as a submodule when pulling.
* Requires JRE 8
* Requires Python 2.7+
* Requires either Visual Studio 2013 (express or community will work and are free), XCode 6 or Android SDK + Eclipse.
